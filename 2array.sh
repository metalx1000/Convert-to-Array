#!/bin/bash
###################################################################### 
#Copyright (C) 2023  Kris Occhipinti
#https://filmsbykris.com

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation version 3 of the License.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
###################################################################### 

#project/source code
#https://gitlab.com/metalx1000/Convert-to-Array
function error(){
    echo "$*"
    exit 1
}

function update(){
    output="/usr/local/bin/2array"
    sudo wget "https://gitlab.com/metalx1000/Convert-to-Array/-/raw/master/2array.sh?inline=false" -O "$output"
    sudo chmod +x "$output"
    exit
}
[[ "$1" == "update" ]] && update

#Dependencies fzf
which fzf &>/dev/null || {
    which apt &>/dev/null || error "fzf is not installed."
    echo "Installing Dependencies"
    sudo apt install fzf
}

[[ $1 ]] && type="$1" || type="$(echo -e "javascript\nphp\nbash"|fzf --prompt "Select Array Type: ")"
[[ $type ]] || exit

[[ $2 ]] && file="$2" || file="$(fzf --prompt "Select a File: ")"
[[ -f $file ]] || error "$file does not exist." 

[[ $3 ]] && name=$3 || name="myArray" 

function javascript(){
    let count="$(wc -l < "$file")"
    let i=1
    echo "$name = ["
    while read -r line 
    do
        let i=$i+1
        if [[ $line ]]
        then
            if [[ $i == $count ]] 
            then
                echo "'$line'"
            else
                echo "'$line',"
            fi
        fi
    done < "$file"
    echo -e "]"
}

function php(){
    let count="$(wc -l < "$file")"
    let i=1
    echo "\$$name = array("
    while read -r line 
    do
        let i=$i+1
        if [[ $line ]]
        then
            if [[ $i == $count ]] 
            then
                echo "'$line'"
            else
                echo "'$line',"
            fi
        fi
    done < "$file"
    echo -e ")"
}

function bash(){
    let count="$(wc -l < "$file")"
    let i=1
    echo "$name=("
    while read -r line 
    do
        let i=$i+1
        if [[ $line ]]
        then
            if [[ $i == $count ]] 
            then
                echo "'$line'"
            else
                echo "'$line' "
            fi
        fi
    done < "$file"
    echo -e ")"
}

[[ "$type" == "javascript" ]] && javascript
[[ "$type" == "php" ]] && php
[[ "$type" == "bash" ]] && bash

